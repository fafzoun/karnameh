from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.db import models


def validate_user_fields(first_name, last_name, mobile, password):
    if not first_name:
        raise ValueError("User must have first name.")
    if not last_name:
        raise ValueError("User must have last name")
    if not mobile:
        raise ValueError("User must have mobile")
    if not password:
        raise ValueError("User must have password")


class UserManager(BaseUserManager):
    def create(self, first_name, last_name, mobile, email=None, date_of_birth=None, password=None, is_admin=False,
               is_staff=False, is_active=True):
        validate_user_fields(first_name, last_name, mobile, password)

        user = self.model(email=self.normalize_email(email))
        user.first_name = first_name
        user.last_name = last_name
        user.mobile = mobile
        user.date_of_birth = date_of_birth
        user.set_password(password)
        user.admin = is_admin
        user.staff = is_staff
        user.active = is_active
        user.save(using=self._db)
        return user

    def create_user(self, first_name, last_name, mobile, email=None, date_of_birth=None, password=None, is_admin=False,
                    is_staff=False, is_active=True):
        return self.create(first_name, last_name, mobile, email, date_of_birth, password, is_admin, is_staff, is_active)

    def create_superuser(self, first_name, last_name, mobile, email=None, date_of_birth=None, password=None):
        return self.create(first_name, last_name, mobile, email, date_of_birth, password, True, True, True)


class User(AbstractUser):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    mobile = models.CharField(max_length=15, unique=True)
    email = models.EmailField(max_length=255, unique=True, null=True, blank=True)
    date_of_birth = models.DateField(null=True, blank=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    USERNAME_FIELD = 'mobile'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    objects = UserManager()

    class Meta(AbstractUser.Meta):
        swappable = 'AUTH_USER_MODEL'

    def __str__(self):
        return self.first_name + ' ' + self.last_name
