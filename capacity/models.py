from django.db import models

from expert.models import Expert


class Capacity(models.Model):
    expert = models.ForeignKey(Expert, on_delete=models.PROTECT)
    date = models.DateField(null=False, blank=False)
    available_capacity = models.IntegerField()

    def __str__(self):
        return str(self.expert) + ' ' + str(self.date)
