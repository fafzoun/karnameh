from capacity.models import Capacity
from expert.models import Expert


def get_capacities(expert_ids, date):
    return Capacity.objects.filter(expert_id__in=expert_ids).filter(expert__is_deleted=0).filter(
        date__gte=date).order_by('date')


def close_all_capacities_for_expert(expert_id):
    Capacity.objects.filter(expert_id=expert_id).filter(available_capacity__gt=0).update(available_capacity=0)


def create_capacity_for_expert(expert_id, date):
    expert = Expert.objects.get(pk=expert_id)
    return Capacity.objects.create(expert_id=expert_id, date=date, available_capacity=expert.daily_capacity)
