from django.contrib import admin

from expert.models import Expert

admin.site.register(Expert)
