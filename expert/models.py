from django.db import models

from karnameh import settings


class Expert(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    daily_capacity = models.IntegerField()
    current_latitude = models.DecimalField(max_digits=10, decimal_places=7)
    current_longitude = models.DecimalField(max_digits=10, decimal_places=7)
    is_deleted = models.IntegerField(default=0)

    def __str__(self):
        return str(self.user)
