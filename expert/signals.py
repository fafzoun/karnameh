from datetime import date, timedelta

from django.db.models.signals import post_save
from django.dispatch import receiver

from capacity.utils import close_all_capacities_for_expert, create_capacity_for_expert
from expert.models import Expert
from karnameh.redis_service import geo_add


@receiver(post_save, sender=Expert)
def handle_expert_update(sender, instance, created, **kwargs):
    if instance.is_deleted > 0:
        close_all_capacities_for_expert(instance.pk)
        return

    geo_add(instance.pk, instance.current_latitude, instance.current_longitude)

    if created:
        tomorrow = date.today() + timedelta(days=1)
        create_capacity_for_expert(instance.pk, tomorrow)
