from datetime import date, timedelta

from capacity.utils import get_capacities, create_capacity_for_expert
from karnameh.redis_service import geo_radius
from request.models import Request


def get_near_expert_ids(latitude, longitude):
    valid_expert_ids = []
    expert_ids = geo_radius(latitude, longitude, 100)
    if expert_ids:
        expert_ids = [expert_id.decode("utf-8") for expert_id in expert_ids]
        for expert_id in expert_ids:
            try:
                int(expert_id)
                valid_expert_ids.append(expert_id)
            except ValueError:
                continue
    return valid_expert_ids


def get_capacity(**kwargs):
    today = date.today()
    expert_ids = get_near_expert_ids(kwargs['latitude'], kwargs['longitude'])
    if expert_ids:
        capacities = get_capacities(expert_ids, today)
        if capacities:
            for capacity in capacities:
                if capacity.available_capacity > 0:
                    return capacity

            first_capacity = capacities[0]
            new_date = first_capacity.date + timedelta(days=1)
            return create_capacity_for_expert(first_capacity.expert_id, new_date)

        return create_capacity_for_expert(expert_ids[0], today)
    return None


def assign_expert(**kwargs):
    request = Request.objects.get(pk=kwargs['request_id'])

    capacity = get_capacity(**kwargs)

    if capacity:
        capacity.available_capacity = capacity.available_capacity - 1
        capacity.save()

        request.date = capacity.date
        request.expert_id = capacity.expert_id
        request.status = Request.Status.NEW
        request.save()

        return

    request.status = Request.Status.FAILED
    request.save()
