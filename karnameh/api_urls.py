from django.urls import path, include

urlpatterns = [
    path("auth/", include('account.urls')),
    path("requests/", include('request.urls')),
]
