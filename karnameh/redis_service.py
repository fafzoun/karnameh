from redis import Redis

conn = Redis()


def geo_add(name, latitude, longitude):
    return conn.geoadd('geo_loc', [str(longitude), str(latitude), name])


def geo_radius(latitude, longitude, distance):
    return conn.georadius("geo_loc", str(longitude), str(latitude), distance, unit="km", sort="ASC")
