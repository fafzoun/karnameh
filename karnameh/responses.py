from rest_framework import status
from rest_framework.response import Response


def success_response(**kwargs):
    return Response(data={"success": True, **kwargs}, status=status.HTTP_200_OK)


def validate_error_response(**kwargs):
    return Response(data={"success": False, **kwargs}, status=status.HTTP_400_BAD_REQUEST)
