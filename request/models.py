from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _

from expert.models import Expert


class RequestManager(models.Manager):
    def create(self, **fields):
        request = self.model()
        request.user = fields['user']
        request.latitude = fields['latitude']
        request.longitude = fields['longitude']
        request.save(using=self._db)
        return request


class Request(models.Model):
    class Status(models.TextChoices):
        PENDING = 'pending', _('Pending')
        NEW = 'new', _('New')
        IN_PROGRESS = 'in_progress', _('In Progress')
        COMPLETED = 'completed', _('Completed')
        CANCELED_BY_USER = 'canceled_by_user', _('Canceled By User')
        CANCELED_BY_EXPERT = 'canceled_by_expert', _('Canceled By Expert')
        FAILED = 'failed', _('Failed')

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    latitude = models.DecimalField(max_digits=10, decimal_places=7)
    longitude = models.DecimalField(max_digits=10, decimal_places=7)
    date = models.DateField(null=True, blank=True)
    expert = models.ForeignKey(Expert, on_delete=models.PROTECT, null=True, blank=True)
    status = models.CharField(max_length=63, choices=Status.choices, default=Status.PENDING)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
