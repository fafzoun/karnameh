from celery.decorators import task
from celery.utils.log import get_task_logger

from expert.utils import assign_expert
from request.models import Request

logger = get_task_logger(__name__)


@task(name='handle_new_request')
def handle_new_request(request_id):
    request = Request.objects.get(pk=request_id)
    return assign_expert(request_id=request.pk, latitude=request.latitude, longitude=request.longitude)
