from django.urls import path

from request.views import RequestView

app_name = "request"
urlpatterns = [
    path("", RequestView.as_view(), name="request")
]
