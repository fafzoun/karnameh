from .models import Request
from .tasks import handle_new_request


def create_request(bind):
    request = Request.objects.create(**bind)
    handle_new_request.delay(request.pk)
