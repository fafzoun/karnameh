from rest_framework.views import APIView

from karnameh.responses import success_response, validate_error_response
from request.serializers import RequestSerializer
from request.utils import create_request


class RequestView(APIView):
    def post(self, request):
        serializer = RequestSerializer(data=request.data)

        if not serializer.is_valid():
            return validate_error_response(errors=serializer.errors)

        bind = serializer.validated_data
        bind['user'] = request.user

        create_request(bind)

        return success_response()
